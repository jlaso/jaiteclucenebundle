JaitecLuceneBundle
=================

-This bundle provides helpers to generate lucene index from your entities.


Installation
============

Add JaitecLuceneBundle to your vendor/bundles/ dir
------------------------------------------

::

    $ git submodule add https://bitbucket.org/jlaso/jaiteclucenebundle.git vendor/bundles/Jaitec/LuceneBundle

or add this to deps

    [JaitecLuceneBundle]    
        git=https://bitbucket.org/jlaso/jaiteclucenebundle.git
        target=/bundles/Jaitec/LuceneBundle

    ; Dependency:
    ;------------
    [ZendSearch]
        git=https://github.com/excelwebzone/zend-search.git
        target=/zend-search

and run 

    $ php bin/vendors install

Add the Jaitec namespace to your autoloader
-------------------------------------------

::

    // app/autoload.php
    $loader->registerNamespaces(array(
        'Jaitec' => __DIR__.'/../vendor/bundles',
        'Zend\\Search'     => __DIR__.'/../vendor/zend-search/',
        // your other namespaces
    );

Add JaitecLuceneBundle to your application kernel
------------------------------------------

::

    // app/AppKernel.php

    public function registerBundles()
    {
        return array(
            // ...
            new Jaitec\LuceneBundle\JaitecLuceneBundle(),
            // ...
        );
    }



Usage
=====

LuceneBundle provides these services to manage your lucene index :

- ``jaitec_lucene``

Sample
======

::

    // app/config/config.yml

    jaitec_lucene:
        analyzer: Zend\Search\Lucene\Analysis\Analyzer\Common\TextNum\CaseInsensitive
        path:     %kernel.root_dir%/cache/lucene/index
        entities:
            user:
                entity: Acme\DemoBundle\Entity\User
                title: name
                short: email
                text: ~
                url: ~
                active: active


        