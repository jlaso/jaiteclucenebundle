<?php

namespace Jaitec\LuceneBundle\Lucene;

use Jaitec\LuceneBundle\Lucene\Lucene;
use Zend\Search\Lucene\Analysis\Analyzer\Analyzer;
use Zend\Search\Lucene\Index\Term;

class LuceneService{

    private $indexPath;
    private $index;
    private $entities;
    private $router;

    public function __construct($entities,$index,\Symfony\Bundle\FrameworkBundle\Routing\Router $router){
        $this->router   = $router;
        $this->entities = $entities;
        $this->indexPath= $index;
        if (file_exists($index)) {
            $this->index = Lucene::open($index);
        } else {
            $this->index = Lucene::create($index);
        }
    }

    /**
     * completly erases the lucene index
     */
    public function eraseIndex(){
        unset($this->index);
        $this->index = Lucene::create($this->indexPath);
    }

    /**
     * returns the subarray config parameters for entity class $item
     * @param $item
     * @return false for no match and subarray with parameters for match
     */
    public function getConfigIndexForEntity($item){
        $className = get_class($item);
        $idx = -1;
        foreach ($this->entities as $keyEntity=>$entity){
            if($entity['entity']===$className){
                $idx = $keyEntity;
                break;
            }
        }
        if($idx==-1) return false;
        else return $idx;
    }

    /**
     * deletes the entity $item from the lucene index
     * @param $item
     * @param bool $optimize, indicates if optimize index
     * @return bool, true if exists, false else
     */
    public function deleteItemFromLuceneIndex($item,$optimize=false){
        $keyEntity = $this->getConfigIndexForEntity($item);
        if($keyEntity!=false){
            $srch = 'entity:'.$keyEntity.' +entity_id:'.$item->getId();
            $hits = $this->find($srch);
            foreach($hits as $hit){
                $this->index->delete($hit->id);
            }
            if($optimize && count($hits)) $this->index->optimize();
            return true;
        }else{

        }
    }

    /**
     * adds a entity $item to the lucene index
     * @param $item
     * @return int|string   1=>ok, -1=>$item not active, other=>class not declared in config.yml
     */
    public function addItemToLuceneIndex($item){

        $keyEntity = $this->getConfigIndexForEntity($item);
        if($keyEntity==false){
            return get_class($item).' not found';
        }
        $entity = $this->entities[$keyEntity];

        $title  = $entity['title'] ?'get'.ucfirst($entity['title']) :'';
        $short  = $entity['short'] ?'get'.ucfirst($entity['short']) :'';
        $text   = $entity['text']  ?'get'.ucfirst($entity['text'])  :'';
        $active = $entity['active']?'get'.ucfirst($entity['active']):'';

        $url    = $entity['url'];
        if($url){
            $args = array();
            //return print_r($url['arguments'],true);
            foreach($url['arguments'] as $arg=>$field){
                if($field){
                    $field = 'get'.ucfirst($field);
                    $args[$arg] = $item->{$field}();
                }
            }
            //return print_r($args,true);
            $url = $this->router->generate($url['path'],$args,true);
        }
        $isActive = $active?$item->{$active}():true;
        if($isActive){

            $_title = $title ?$item->{$title}():'';
            $title  = $this->sanitize($_title);
            $_short = $short ?$item->{$short}():'';
            $short  = $this->sanitize($_short);
            $_text  = $text  ?$item->{$text}() :'';
            $text   = $this->sanitize($_text);

            $document = new Document();
            $document->addField(Field::keyword  ('entity'    ,$keyEntity                 ));
            $document->addField(Field::keyword  ('entity_id' ,$item->getId()             ));
            // the title whithout extrange chars to can search
            $document->addField(Field::text     ('_title'    ,$_title            ,'utf-8'));
            // now the original text to can show in template
            $document->addField(Field::unIndexed('title'     ,$title             ,'utf-8'));
            // idem for short
            $document->addField(Field::text     ('_short'    ,$_short            ,'utf-8'));
            $document->addField(Field::unIndexed('short'     ,$short             ,'utf-8'));
            $document->addField(Field::text     ('url'       ,$url?$url:''               ));
            $document->addField(Field::unStored ('_text'     ,$_text             ,'utf-8'));
            $this->index->addDocument($document);

            return 1;

        }else{

            return -1;

        }

    }


    /**
     * updateIndex
     *
     * A convience function to commit and optimize the index
     */
    public function updateIndex()
    {
        $this->index->commit();
        $this->index->optimize();
    }

    public function find($query)
    {
        return $this->index->find($this->sanitize($query));
        //return call_user_func_array(array($this->index, 'find'), func_get_args());
    }

    public function updateDocument($document)
    {
        $this->addDocument($document);
    }

    public function deleteDocument($document)
    {
        // Search for documents with the same Key value.
        $term = new Term($document->key, 'key');
        $docIds = $this->index->termDocs($term);

        // Delete any documents found.
        foreach ($docIds as $id) {
            $this->index->delete($id);
        }
    }


    /**
     * get the actual lucene index path
     * @return \Zend\Search\Lucene\IndexInterface
     */
    public function getIndex() {
        return $this->index;
    }

    /**
     * gets the entity types declared in config.yml
     * @return array
     */
    public function getAllowedEntityTypes(){
        $result = array();
        foreach ($this->entities as $keyEntity=>$entity){
            $result[] = $keyEntity;
        }
        return $result;
    }

    /**
     * sanitize string
     * @param $string
     * @return string
     */
    public function sanitize($string){
        $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $string = utf8_decode($string);
        $string = strtr($string, utf8_decode($a), $b);
        $string = strtolower($string);
        return utf8_encode($string);
    }

    /**
     * Converts the long entity name like Acme\DemoBundle\Entity\User into AcmeDemoBundle:User for use with repository
     * @param $str
     * @return string
     */
    protected function toCompactEntityName($str){

        if(preg_match_all("|^(.*)\\\(.*)Bundle\\\Entity\\\(.*)$|i",$str,$matches,PREG_SET_ORDER)){

            $matches = $matches[0];
            return $matches[1].$matches[2].'Bundle:'.$matches[3];

        }else{

            return $str;

        }
    }

    /**
     * Converts the long entity name like Acme\DemoBundle\Entity\User into DemoBundle:User for use with repository
     * @param $str
     * @return string
     */
    protected function toCompactEntityNameWithoutProjectName($str){

        if(preg_match_all("|^(.*)\\\(.*)Bundle\\\Entity\\\(.*)$|i",$str,$matches,PREG_SET_ORDER)){

            $matches = $matches[0];
            return /*$matches[1].*/$matches[2].'Bundle:'.$matches[3];

        }else{

            return $str;

        }
    }

    /**
     * reindex all entities
     * @return array of results
     */
    public function reindexLuceneAllEntities($entityManager,$eraseIndex=true){

        //$em = $this->get('Doctrine')->getEntityManager();

        if($eraseIndex) $this->eraseIndex();

        $entities = $this->entities;

        $output = array();

        foreach ($entities as $entityName=>$entity){

            try{
                $repo = $this->toCompactEntityName($entity['entity']);
                $items = $entityManager->getRepository($repo)->findAll();
            }catch(\Exception $exception){
                $repo = $this->toCompactEntityNameWithoutProjectName($entity['entity']);
                $items = $entityManager->getRepository($repo)->findAll();
            }

            $output[] = '['.$entityName.']  '.count($items).' items.';

            foreach($items as $item){
                $result = $this->addItemToLuceneIndex($item);
                if($result!=1){
                    if($result==-1){
                        $output[] = $item->getId().' -> omitted because is not active';
                    }else{
                        $output[] = $item->getId().' -> '.$result;
                    }
                }
            }

        }
        $this->updateIndex();

        return $output;
    }

    /**
     * removes and add the item to update it
     * @param $item
     * @return int|string
     */
    public function updateItemInLuceneIndex($item){
        $this->deleteItemFromLuceneIndex($item);
        return $this->addItemToLuceneIndex($item);
    }
}