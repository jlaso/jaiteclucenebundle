<?php
namespace Jaitec\LuceneBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class ReindexCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            ->setName('jaitec:lucene:reindex')
            ->setDescription('regenerates lucene index from entities configured')
            ->addArgument(
                'host',
                InputArgument::OPTIONAL,
                'host for generated urls'
            )
            ->addOption(
                'append',
                null,
                InputOption::VALUE_NONE,
                'recreates the index without erase the old lucene index'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting reindex process');
        $em = $this->getContainer()->get('Doctrine')->getEntityManager();

        // if less host blank use this schema when show the url in template
        // app.request.scheme ~  '://' ~  app.request.host ~ hit.url
        $host = $input->getArgument('host');
        $this->getContainer()->get('router')->getContext()->setHost($host);

        $index = $this->getContainer()->get('jaitec_lucene');

        $eraseIndex = $input->getOption('append')?false:true;
        $msgs = $index->reindexLuceneAllEntities($em,$eraseIndex);

        foreach ($msgs as $msg){

            $output->writeln($msg);

        }

    }
}