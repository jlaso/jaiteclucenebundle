<?php

/**
 * (c) Joseluis Laso <wld1373@gmail.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Jaitec\LuceneBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;


class JaitecLuceneExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $config = array();
        foreach ($configs as $subConfig) {
            $config = array_merge($config, $subConfig);
        }
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        if (!count($config)) {
            $loader->load('config.yml');
        }else{
            $container->setParameter('jaitec_lucene', $config);
        }
        //var_dump($container->getParameter('jaitec_lucene'));
        $container->setParameter('jaitec_lucene.analyzer',   $config['analyzer']);
        $container->setParameter('jaitec_lucene.index_path', $config['path']);
        $container->setParameter('jaitec_lucene.entities',   $config['entities']);
    }
}
